﻿/****************************************************************************************************
* POV-Ray Rendering Trigger Software
* Author: Devon Smith
* Program Description: This appplication finds the version of POV-Ray installed and the Windows 
* System's current "Program Files" directory. This then will run the POV-RAY application with the file
* passed to it.
*
* This application is intended to work with the second lab in CS360 at Western Oregon University. This
* application acts as a shim between the Windows system and Python. This will retrieve environment 
* variables and will run POV-Ray.
*
*****************************************************************************************************/

using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;


namespace POV_Render
{
    class Program
    {
        static void Main(string[] args)
        {
            string command = "";
            string arguments = "";

            // Version Check for POV-RAY
            // This code will check the version of POV-Ray installed and create a string that can be used
            // to run the currently installed version of POV-Ray automatically.

            // This code will get the evironment variable for "Program Files"
            // This will allow the application to find the install of POV-Ray even if the "Program Files" directory has been moved from the
            // default location. 
            string programsDir = Environment.GetEnvironmentVariable("ProgramFiles");

            // Read the current version registry key.
            RegistryKey reg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\POV-Ray\CurrentVersion\Windows");
            // Create a string from the VersionNo64 value. Note: This application only works with Windows 64.
            // Do not attempt to use this with a 32-bit version of Windows.
            string prVersion = reg.GetValue("VersionNo64").ToString();
            // Remove the minor version number from the version.
            prVersion = prVersion.Remove(prVersion.Length - 2);
            // Tell the user what version was detected. This is an optional flag.
            //Console.WriteLine("Pov-Ray Version Detected: " + prVersion + "\nRendering file...");

            // If for some reason POV-RAY is in another location you and create an override.txt file and it will launch that application instead.
            if (File.Exists("override.txt")) {
                //Create a stream reader for a txt file for a command override
                StreamReader sr = new StreamReader("override.txt");
                // Read the first line of the override file for the application command.
                command = sr.ReadLine();
                // read the second line of the override file for the command arguments.
                arguments = sr.ReadLine();
            }
            // If there was not override file, we're going to run POV-RAY from the default folder for that version.
            // At the time of creation POV-RAY was using c:\program files\POV-RAY\v#.#\... so this assumes that pattern.
            // There is a chance that they may change that when they create the next version of POV-RAY, but for now this
            // will work and autodetect the installed location assuming the user installed the files in the default location.
            // Otherwise this can be overridden with a txt file.
            else {
                // the command string from the install directory and the version.
                command = programsDir + "\\POV-Ray\\v" + prVersion + "\\bin\\pvengine.exe";
                // The command-line arguments being used for the python movie rendering script.
                arguments = " +A0.5 +H720 +W1280 /EXIT /RENDER ";
            }
            
            // If the user provided a file, run the application. If not, exit.
            if (args.Length >= 1) { 
                try{
                    // Create the new process for POV-RAY.
                    Process prApplication = new Process();

                    // Set the application file name.
                    prApplication.StartInfo.FileName = command;

                    // Because of some string conversion problems between python and the stdout to the 
                    // command prompt this can pass a single string a individual strings. So if there
                    // are spaces in the file name for the POV file this will concatenate them into a 
                    // single string.

                    string argString = "";
                    foreach (string arg in args){
                        argString += arg + " ";
                    }

                    // Set application command-line arguments
                    prApplication.StartInfo.Arguments = arguments + "\'" + argString + "\'";

                    // A diagnostic string to check the file and arguments being passed.
                    //Console.WriteLine(prApplication.StartInfo.FileName + prApplication.StartInfo.Arguments);

                    // Hide the application window
                    prApplication.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                    // Start the application
                    prApplication.Start();
                    // Wait for the application to exit before closing.
                    prApplication.WaitForExit();
                }
                catch{
                    //Let the user know what went wrong.
                    Console.WriteLine("Failed to start application.");
                    //wait for user confirmation
                    //Console.ReadKey();
                }
            }
            // The file was not specified.
            else {
                Console.WriteLine("Usage: POV_Render.exe <filename>.\nPress any key to exit.");
                Console.ReadKey();

            }
        }
    }
}
