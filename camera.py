#!/usr/bin/python

#############################################################################
##                                                                         ##
## CS360 - Programming Languages - Lab Two                                 ##
## Devon Smith                                                             ##
## Western Oregon University - Fall 2016                                   ##
## This file: camera.py creates camera paths for animation frames.         ##
##                                                                         ##
#############################################################################

## This module creates the cartesian paths for the camera. It provides
## functions that create lists that contain tuples of 3D vectors that
## represent the camera's position for each animation frame.

import math  # math tools required for sin, cos, and radians functionality

# Module Variables.
increment = None
elevation = None
y_start = None

#Not used, but available.
x_start = None
Z_start = None


# This functions will create a circle divided into the number of frames you
# want to have the video run it will return a list of tuples that contain the
# x and y coordinates of the circle.
def cartesian_circle(frames, radius, height):
    # Get module variables for this scope.
    global increment
    global elevation
    global y_start

    # How high should the camera sit?
    y_start = height

    # A list to store the tuple values for the coordinates.
    ret_list = []
    # Determine the number of degrees needed to rotate around the object
    # in the number of frames specified.
    increment = 360.0/frames
    elevation = 1.5*radius/frames
    # Since we're moving around the object in a 3d space we need to move the x
    # and z axis locations.
    for i in range(frames+1):
        x = radius * math.cos(math.radians(increment*i))
        z = radius * math.sin(math.radians(increment*i))
        y = y_start
        ret_list += [[x, y, z]]
    return ret_list

    # This functions will create a helix divided into the number of frames you
    # want to have the video run it will return a list of tuples that contain
    # the x and y coordinates of the circle.


def cartesian_helix(frames, radius):
    # Get module variables for this scope.
    global increment
    global elevation

    # A list to store the tuple values for the coordinates.
    ret_list = []
    # Determine the number of degrees needed to rotate around the object
    # in the number of frames specified.
    increment = 360.0/frames
    elevation = 1.5*radius/frames
    # Since we're moving around the object in a 3d space we need to move the x
    #  and z axis locations.
    for i in range(frames+1):
        x = radius * math.cos(math.radians(increment*i))
        z = radius * math.sin(math.radians(increment*i))
        y = elevation*i
        ret_list += [[x, y, z]]
    return ret_list
