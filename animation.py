#!/usr/bin/python

#############################################################################
##                                                                         ##
## CS360 - Programming Languages - Lab Two                                 ##
## Devon Smith                                                             ##
## Western Oregon University - Fall 2016                                   ##
## This file: The driver for the project, animation.py                     ##
##                                                                         ##
#############################################################################


#############################################################################
##                                                                         ##
## Produce Requirements:                                                   ##
##     * Code Renders a Single Image : Code will render all required       ##
##       images for video. tested to 300 images.                           ##
##     * Loop Moves the Camera in a helical pattern.                       ##
##       Helix is defined by the set radius and the number of frames. The  ##
##       radius determines the radius of the camera and the height of the  ##
##       helix is 1.5 times the defined radius. The number of frames       ##
##       will move through the helical camera loop.                        ##
##     * Images Encode to movie format and outputs an AVI file.            ##
##       By default will output a MP4 contained file, AVI format available ##
##       through the "--avi-video" command line switch.                    ##
##     * Uses two or more modules:                                         ##
##       Contains two modules including module variables and functions.    ##
##       The camera module which provides the functions to create          ##
##       cartesean coordinates for the camera posistions and provides a    ##
##       circular motion function and a helical motion function. The video ##
##       module provides the function to create the video file. This       ##
##       design allows the project to be extended easily and will allow    ##
##       the definition of new movements for the camera.                   ##
##     * Created a class that creates an object                            ##
##       The class creates a command collection object that will allow     ##
##       commands for the application to be stored and used elsewhere in   ##
##       the application.                                                  ##
##     * Class uses:                                                       ##
##           - An instance method: Setters and getters                     ##
##           - An instance variable: All command variables are instance    ##
##             variables.                                                  ##
##           - A class variable: The cleanup variable is a class variable. ##
##           - Constructor Method: A constructor is defined for the class. ##
##     * Uses Lists, Tuples, and Dictionaries                              ##
##           - List: A list of coordinates for the camera are created.     ##
##           - Tuples: The camera position is a 3D vector with X,Y, and Z  ##
##                     which are stored as tuples inside the camera        ##
##                     coordinate list.                                    ##
##           - Dictionary: Used as part of the keyword argument (since it  ##
##                         generates a dictionary as part of its           ##
##                         function), and used to store command variables  ##
##                         for retrieval.                                  ##
##     * Keyword Arguments: The video funcion will use a keyword           ##
##                          dictionary and will look for any key in that   ##
##                          dictionary that contains "noclean" which will  ##
##                          prevent the deletion of the PNG frames used to ##
##                          create the video file, and "avi" will make an  ##
##                          AVI file.                                      ##
##                                                                         ##
#############################################################################

#############################################################################
##                                                                         ##
## Notes: I have provided an Audio Video Interleave file as specified by   ##
##        the project instructions. However, This application does not     ##
##        create AVI files by default, but can be set to create them using ##
##        ffmpeg.The AVI container format has been replaced since its      ##
##		  frame encoding.There have been modifications to this but         ##
##		  generally speaking AVI support is limited. This application      ##
##        produces MP4 container files that are encoded using H.264 using  ##
##        the x264 encoder in ffmpeg.                                      ##
#############################################################################

## TODO: Add the ability to specify a audio file for the video being created.

import sys  # System tools.
import re  # Retular Expression
import os  # Operating system tools.
import shutil  # Shell utilities, used for file manipulation.
import subprocess  # enabled the creation of subprocess with POpen

## IMPORTED MODULES CREATED FOR THIS PROJECT ##
import mkvideo  # Get the video creation module.
import camera  # get the camera movement module.


## CLASSES ##
# This class is a command collection class. It can be used to create a
# command collection object. This command collection class could be added
# to another collection that could contain the configuration information
# for multiple files. Currently this application is not setup to do this,
# but with this class can be easily extended to process multiple POV files
# to create videos for them. This could also be done with a dictionary.
class Command_Collection:
    # Class Variables: Variables not defined in the constructor or method
    #                  are class variables. according to the language
    #                  specification. there could also be a instance
    #                  variable with the same name that will not be shared
    #                  between instances.

    cleaning_preference = 0

    # A constructor
    # This allows the creation of the class and requires all off the
    # instance variables be set.

    def __init__(self, pov, fcount, r, fps):
    # Instance Variables
    # A dictionary that stores the command information.
    # This could be broken down into a number of variables instead
    # of this ad-hoc sequence, but this data structure is extensible
    # and provides little overhead to this class.
        self.collection = dict()
        self.collection['pov_file'] = pov
        self.collection['frame_count'] = fcount
        self.collection['circle_radius'] = r
        self.collection['frames_per_second'] = fps

    # Class Methods:
    @classmethod
    def set_clean(self, mode):
        if mode > 0:
            self.cleaning_preference += 1

    @classmethod
    def get_clean(self):
        return self.cleaning_preference

# Methods:
    # path to the POV File (pov_file)
    def set_pov(self, filename):
        self.collection['pov_file'] = filename

    def get_pov(self):
        return self.collection.get('pov_file')

    # The number of frames to create for the video (frame_count)
    def set_fcount(self, count):
        self.collection['frame_count'] = count

    def get_fcount(self):
        return self.collection.get('frame_count')
    # Circle Radius

    def set_radius(self, radius):
        self.collection['circle_radius'] = radius

    def get_radius(self):
        return self.collection.get('circle_radius')

    # The FPS of the video that will be created.
    def set_fps(self, fps):
        self.collection['frames_per_second'] = fps

    def get_fps(self):
        return self.collection.get('frames_per_second')


## FUNCTIONS ##
def run_application(filename):
    '''
    The code below this point matches directly the specification for the
    project in the lab description. This however has a number of limitations.:
          * If the file path does not match on another computer it will not
            work.
          * If the user has multiple drives and the Program Files directory is
    redirected, it will not work. This code was replaced with a C# application
    that will call POV-Ray from the location depending on where the Program
    Files are stored, and which version of POV-Ray is installed. For
    information about how the C# application works please see the included
    source code in: POV-Render.cs
    '''

    # Command that meets specifically the requirements of the lab:

    command = ('\"C:\\Program Files\\POV-Ray\\v3.7\\bin\\pvengine.exe\" +A0.5'
               + '+H720 +W1280 /EXIT /RENDER ')
    command += "\'" + filename + "\'"

    '''Here is the call to the external application that can find POV-Ray '''
    # Specify the application we want to run.
    # POV-Render application written in C# to automatically find the correct
    # version and install location.
    # command = r"Pov-Render.exe" + " " + filename

    # "C:\Program Files\POV-Ray\v3.7\bin\pvengine.exe"
    #Diagnostic Print Code:
    #print command

    proc = subprocess.Popen(command, shell=True, stderr=subprocess.PIPE)
    while True:
        out = proc.stderr.read(1)
        if out == '' and proc.poll() is not None:
            break
        if out != '':
            sys.stdout.write(out)
            sys.stdout.flush()


## MAIN ##
def main():
    # Pattern for processing command-line arguments from python documentation
    # and demonstrated in the Google Python Quick class.
    # Get the command-line arguments from python
    args = sys.argv[1:]

    ### Add a for/while loop here to process arguments for multiple files.
    ### Then we can create multiple command collections that can process
    ### multiple files at a time.

    # If there were no command arguments then there is nothing
    # we can do right now. Tell the user how to use the script.
    if len(args) == 0:
        print ('usage: animate.py [--frames <integer>] [--avi-video]'
               + '[--no-clean] <filename.pov>')
        sys.exit(1)

    # Did the user specify the number of frames?
    frames = 0
    if args[0] == '--frames':
        # get the number of frames to render as an integer.
        frames = int(args[1])
        # remove what we got from the arg collection.
        del args[0:2]

    # Did the user want an AVI file instead of a MP4 file?
    avi_file = 0
    if args[0] == '--avi-video':
        avi_file += 1
        # remove this processed flag from the args sequence.
        del args[0:1]

    # Did the user want us to clean up after them?
    noclean = 0
    if args[0] == '--no-clean':
        noclean += 1  # we don't want the directory cleaned.
        del args[0:1]  # remove processed arguments

    # Since we're editing the file we need to know if we've stored a backup of
    #  the original file.
    # original_saved = 0
    # Have we added the object from the object file to the animation?
    object_added = 0

    '''
    THIS IS WHERE YOU CAN CHANGE SETTINGS FOR THE VARIOUS ITEMS IN THIS SCRIPT
    '''

    # This stores a dictionary of all the commands that we want to run
    # exernally. We'll store the definition of a command and will retrived it
    # from this dictionary

    file = str(args[0])
    # Instance of the command collection class for the file we're working with
    # FILENAME, NUMBER OF FRAMES, RADIUS OF CIRCLE, FRAME RATE
    if (frames <= 0):
        pov_commands = Command_Collection(file, 20, 10, 60)
    else:
        pov_commands = Command_Collection(file, frames, 10, 60)
    #TODO: Potentially add a means to defined radius and frame rate via
    #      command-line.
    pov_commands.set_clean(noclean)

    # camera position list.
    pos_list = []

    pos_list = camera.cartesian_helix(pov_commands.get_fcount(),
                                      pov_commands.get_radius())

    # if we haven't saved the original document, we need to do that.
    # if there is a previous backup, we need to kill it.
    if os.path.exists('./backupfile.pov'):
        os.remove('./backupfile.pov')
    # Then make a backup of the file.
    shutil.copyfile(pov_commands.get_pov(), "./backupfile.pov")

    #A counter to use inside the loop. Starts at 1.
    counter = float(1.0)
    for i in range(pov_commands.get_fcount()):
        ## Things to edit the POV FIle ##
        # Regex: '\bcamera{ \b.+\}'
        # String matching:
        # camera{ perspective location <5,5,-20> look_at <0,0,0> }

        # Get the coordinates from the position list. Unpack them from the
        # list to something that can be used.
        x, y, z = pos_list[i]

        camera_string = ('camera{ perspective location <' + str(x) + ','
                         + str(y) + ',' + str(z) + '>' + 'look_at <0,0,0> }')

        # Get the object we're going to add to the scene from a text file
        # containing the object's SDL code. This should be a complete parsable
        # snippit. Currently this appplication will only move the object
        # along the x-axis, but could be confiured to look at the first line
        # of a text document for the movement path definition.
        file = open(pov_commands.get_pov(), 'r+')
        pov_document = file.read()
        object_document = ''
        if os.path.exists('.\\object.txt'):
            ofile = open('.\\object.txt', 'r')
            object_document = ofile.read()

        # Find and replace the line in the document.
        pov_document = re.sub(r'\bcamera{ \b.+\}',
                              camera_string, pov_document)

        # Add the object file to the end of the POV file.
        movement = 0
        if (i >= pov_commands.get_fcount()/4):
            # how far should the object move? This should be moved outside the
            # loop at some point.
            movement = (float(25.0
                        / (0.75*float(pov_commands.get_fcount())))
                        * counter)

            # If an object file was supplied, and we haven't added this code to
            # the file yet:
            if object_document:
                if object_added == 0:
                    pov_document += object_document
                    object_added = 1
            move = str(50+movement)
            move_line = 'translate <' + move + ',0,0> //movebullet'
            pov_document = re.sub(r'\btranslate <\b.+\/\/\bmovebullet\b',
                                  move_line, pov_document)
            counter += 1.0
            del move

        # go to the first line in the file...
        file.seek(0)
        # write out new stuff to the document.
        file.write(pov_document)
        #remove all the things below where we are in the file.
        file.truncate()
        file.close()

        ## RUNNING POV-RAY ##
        run_application(pov_commands.get_pov())

        ## COPY FILE TO NAME ##

        # What is the index of the image count?
        imagecount = i
        # Here is where the string that will be the name of the image file is
        # built.
        # Get the name of the POV file from the command collection.
        imgname = pov_commands.get_pov()
        # Since it is a POV file, remove the last three characters in the
        # file name and replace them with PNG for the image file.
        imgname = imgname[:-3] + "png"
        # Image file name = imgX.png, where x is the integer representation of
        # the file created. This will act as a frame index for when the frames
        # are passed on to ffmpeg.
        newimage = 'img' + str(imagecount) + '.png'

        # If the directory was not cleaned, we're going to replace the file.
        if os.path.exists(newimage):
            os.remove(newimage)  # remove the single file from the directory.
        # move the file from the output file name to the sequential file name.
        os.rename(imgname, newimage)
    # Since we edit the file and add an object ot the file with this script.
    # We need to restore the file to its original state:
    #Put the original contents back in the file.
    if os.path.exists(pov_commands.get_pov()):
        os.remove(pov_commands.get_pov())
    shutil.copyfile("./backupfile.pov", pov_commands.get_pov())

    # When all the files are made: make a video!
    # Did the user want the files to be cleaned after the video was created?
    # If they did or did not specify, we'll clean it up. If the said they
    # didn't want it cleaned, this will pass the keyword argument to the
    # make_video function. This is kinda gross, but switch doesn't strictly
    # exist in python.
    if pov_commands.get_clean() > 0:
        # avi file
        if (avi_file > 0):
            mkvideo.make_video(pov_commands.get_fcount(), clean="noclean",
                               video="avi")
        # mp4 file
        else:
            mkvideo.make_video(pov_commands.get_fcount(), clean="noclean")
    #keep it clean!
    else:
        #avi file
        if (avi_file > 0):
            mkvideo.make_video(pov_commands.get_fcount(), video="avi")
        #mp4 file.
        else:
            mkvideo.make_video(pov_commands.get_fcount())

if __name__ == '__main__':
    main()
