#!/usr/bin/python

#############################################################################
##                                                                         ##
## CS360 - Programming Languages - Lab Two                                 ##
## Devon Smith                                                             ##
## Western Oregon University - Fall 2016                                   ##
## This file: mkvideo.py module for creation of video files.               ##
##                                                                         ##
#############################################################################

## This module contains the functionality that creates the videos. This will
## call ffmpeg and manage the files that were created by the POV-Ray
## application loop.

import sys  # System tools.
import os  # Operating system tools.
import subprocess  # enabled the creation of subprocess with POpen


# places to store some information
#These variables are available for this module scope.
clean = None  # Should we clean the files up afterward?
avi = None  # should be make an AVI file?


# This will call the video creation tool for this script.
# This function will take the keyword argument clean=false to stop cleanup.
def make_video(fps, **kwargs):

    #initialize the variables for the module scope.
    # go get 'em
    global clean
    global avi

    #initialize
    clean = 0
    avi = 0

    # Look for the noclean and avi keyword in the kwargs dictionary.
    for k in kwargs:
        print ("Key received: " + str(kwargs.get(k)))
        if kwargs.get(k) == "noclean":
            clean += 1
        elif kwargs.get(k) == "avi":
            avi += 1

    # Remove the old video file, only remove the old file if the extension is
    # the same.
    if (avi > 0):
        if os.path.exists(".\\video.avi"):
            os.remove('.\\video.avi')
    else:
        if os.path.exists(".\\video.mp4"):
            os.remove('.\\video.mp4')

    # Call the video creation tool. In this case I'm using ffmpeg because it's
    # simple.
    if avi > 0:
        command = (r".\\ffmpeg\\bin\\ffmpeg -start_number 0 -i img%01d.png"
                   + "-c:v libx264 -r" + " "
                   + str(fps) + " " + "-pix_fmt yuv420p video.avi")
    else:
        command = (r".\\ffmpeg\\bin\\ffmpeg -start_number 0 -i img%01d.png"
                   + " -c:v libx264 -r" + " " + str(fps) + " "
                   + "-pix_fmt yuv420p video.mp4")

    # the command run to remove all the png files created by pov ray. This is
    # not cross platform.
    cleanup = r'del .\*.png'

    # Run the player command.
    proc = subprocess.Popen(command, shell=True, stderr=subprocess.PIPE)
    while True:
        out = proc.stderr.read(1)
        if out == '' and proc.poll() is not None:
            break
        if out != '':
            sys.stdout.write(out)
            sys.stdout.flush()
    # Keep it clean!
    # did the user want to keep it clean or do all the hard work themself?
    # in this this case I don't care about passing through messages from del..

    # if the user wanted to keep the image files, tell them about it:
    if clean > 0:
        print ("Image files will not be deleted...")
    else:
        # run the cleanup process.
        subprocess.Popen(cleanup, shell=True)
